import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../service/movieService";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";
const onChange = (key) => {
  console.log(key);
};
export default function MovieTab() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderDanhSachPhimTheoCumRap = (cumRap) => {
    return (
      <div>
        {cumRap.danhSachPhim.map((movie) => {
          return <MovieItemTab movie={movie} />;
        })}
      </div>
    );
  };
  const renderCumRapTheoHeThongRap = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap) => {
      return {
        label: (
          <div className="w-40">
            <h5>{cumRap.tenCumRap}</h5>
            <p className="truncate">{cumRap.diaChi}</p>
          </div>
        ),
        key: cumRap.maCumRap,
        children: (
          <div style={{ height: 400, overflowY: "scroll" }}>
            {renderDanhSachPhimTheoCumRap(cumRap)}
          </div>
        ),
      };
    });
  };
  const renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{ height: 400 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThongRap(heThongRap)}
          ></Tabs>
        ),
      };
    });
  };
  return (
    <div className="container mx-auto mt-20">
      <Tabs
        style={{ height: 400 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
