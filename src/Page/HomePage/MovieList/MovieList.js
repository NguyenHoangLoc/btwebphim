import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function MovieList({ movieArr }) {
  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
      return (
        <Card
          hoverable
          style={{ width: 240 }}
          cover={
            <img
              className="h-60 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            title={<h2 className="text-blue-500 h-10">{item.tenPhim}</h2>}
          />
          <NavLink
            className="bg-red-600 px-5 py-2 text-white rounded"
            to={`/detail/${item.maPhim}`}
          >
            Xem Chi Tiết
          </NavLink>
        </Card>
      );
    });
  };
  return (
    <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5">
      {renderMovieList()}
    </div>
  );
}
