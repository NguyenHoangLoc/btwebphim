import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default function DetailPage() {
  let params = useParams();
  console.log("params: ", params);
  return (
    <div>
      <h2 className="text-red-600 text-center font-black">
        Mã Phim: {params.id}
      </h2>
    </div>
  );
}
