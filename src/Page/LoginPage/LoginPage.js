import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "../../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constant/userConstant";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/88642-merry-christmas.json";
import {
  setUserAction,
  setUserActionService,
} from "../../redux/action/userAction";
export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  //Đăng nhập
  const onFinish = (values) => {
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log(res);
        message.success(" Đăng Nhập Thành Công ");
        dispatch(setUserAction(res.data.content));
        userLocalService.set(res.data.content);
        setTimeout(() => {
          //window.location =>>load lại trang
          // window.location.href = "/";
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishReduxThunk = (values) => {
    //định nghĩa hàm navigate
    const handleNavigate = () => {
      setTimeout(() => {
        // window.location.href = "/";
        navigate("/");
      }, 1000);
    };
    dispatch(setUserActionService(values, handleNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex justify-center items-center bg-blue-500">
      <div className="container p-5 bg-white flex">
        <div className="w-1/2">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2 pt-28">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              className="text-center"
              wrapperCol={{
                // offset: 12,
                span: 24,
              }}
            >
              <Button
                className="bg-blue-500 hover:text-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
