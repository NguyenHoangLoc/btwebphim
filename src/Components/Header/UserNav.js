import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });
  console.log("user: ", user);
  const handleLogout = () => {
    //xóa dữ liệu từ localStore
    userLocalService.remove();
    //đá user ra trang login
    // window.location.href = "/login";
    //reload trang chủ
    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      //đã đăng nhập
      return (
        <>
          <span>{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng Nhập
          </button>
          <button className="border-2 border-black px-5 py-2 rounded">
            Đăng Kí
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}
