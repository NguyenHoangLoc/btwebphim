import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import FooterDesktop from "./FooterDesktop";
import FooterMobile from "./FooterMobile";
import FooteTablet from "./FooterTablet";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>
      <Tablet>
        <FooteTablet />
      </Tablet>
      <Mobile>
        <FooterMobile />
      </Mobile>
    </div>
  );
}
