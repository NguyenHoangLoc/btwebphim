import { postLogin } from "../../service/userService";
import { SET_USER_LOGIN } from "../constant/userConstant";
import { message } from "antd";

export const setUserAction = (value) => {
  return {
    type: SET_USER_LOGIN,
    payload: value,
  };
};
export const setUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        message.success(" Đăng Nhập Thành Công ");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };
};
